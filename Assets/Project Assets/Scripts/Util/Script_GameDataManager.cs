﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stores main game related data
public class Script_GameDataManager : MonoBehaviour
{
    #region Singleton
    public static Script_GameDataManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    public List<CharacterSkin> outfitList;
    public List<CharacterSkin> mouthList;
    public List<CharacterSkin> eyeList;
}
