﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using DG.Tweening;

//Handles screenshot functions
public class Script_ScreenShotManager : MonoBehaviour
{
    [SerializeField]
    private int screenshotWidth = 100;

    [SerializeField]
    private int screenshotHeight = 100;

    [SerializeField]
    private int screenshotOffsetX = 100;

    [SerializeField]
    private int screenshotOffsetY = 100;

    [SerializeField]
    private Camera screenshotCamera;


    [SerializeField]
    private SpriteRenderer screenshotFrame;

    [SerializeField]
    private SpriteRenderer screenshotSprite;

    [SerializeField]
    private Script_Gallery screenShotGallery;

    [SerializeField]
    private AudioClip cameraClip;


    #region AnimationParameters
    [Header("Animation Settings")]

    [SerializeField]
    private float screenshotYMoveTime = 0.8f;

    [SerializeField]
    private float screenshotYMoveDelay = 0.5f;

    [SerializeField]
    private float screenshotYMoveLength = 13f;

    #endregion

    private bool takeScreenshotOnNextFrame = false;

    private void LateUpdate()
    {
        //Take the screenshot once the flag is set after rendering
        if (takeScreenshotOnNextFrame)
        {
            takeScreenshotOnNextFrame = false;

            RenderTexture _renderTexture = new RenderTexture(Screen.width, Screen.height, 24);
            screenshotCamera.targetTexture = _renderTexture;

            Texture2D _screenShot = new Texture2D(screenshotWidth, screenshotHeight, TextureFormat.RGB24, false);
            screenshotCamera.Render();
            RenderTexture.active = _renderTexture;
            _screenShot.ReadPixels(new Rect(screenshotOffsetX, screenshotOffsetY, screenshotWidth, screenshotHeight), 0, 0);

            screenshotCamera.targetTexture = null;
            RenderTexture.active = null;
            Destroy(_renderTexture);

            byte[] bytes = _screenShot.EncodeToPNG();

            //If building user streaming assets folder instead
            string _filePath = Application.dataPath + "/Screenshots/" + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png";
            File.WriteAllBytes(_filePath, bytes);

            Debug.Log("Screenshot successful");

            UnityEditor.AssetDatabase.Refresh();

            Script_UserDataManager.Instance.AddScreenShots(_filePath);

            StartCoroutine(WaitAndShowScreenshot(_filePath));
        }
    }

    private void TakeScreenShot()
    {
        //Set the flag to take screenshot
        takeScreenshotOnNextFrame = true;

        Script_AudioManager.Instance.PlayAudio(cameraClip, 1);
    }

    IEnumerator WaitAndShowScreenshot(string filePath)
    {
        screenshotFrame.enabled = true;
        yield return new WaitForEndOfFrame();

        screenshotSprite.sprite = LoadNewSprite(filePath);
        screenshotSprite.enabled = true;

        Vector3 _initialPos = screenshotFrame.transform.localPosition;
        screenshotFrame.transform.DOLocalMoveY(screenshotFrame.transform.localPosition.y - screenshotYMoveLength, screenshotYMoveTime).SetEase(Ease.InBack).SetDelay(screenshotYMoveDelay).OnComplete(() =>
        {
            screenshotFrame.enabled = false;
            screenshotSprite.enabled = false;
            screenshotFrame.transform.position = _initialPos;
        });
    }

    private Sprite LoadNewSprite(string filePath, float pixelsPerUnit = 100.0f)
    {
        Sprite _newSprite = null;
        Texture2D _spriteTexture = LoadTexture(filePath);
        _newSprite = Sprite.Create(_spriteTexture, new Rect(0, 0, _spriteTexture.width, _spriteTexture.height), new Vector2(0.5f, 0.5f), pixelsPerUnit);

        if (_newSprite != null)
        {
            screenShotGallery.AddScreenShot(_newSprite);
        }
        return _newSprite;
    }

    private Texture2D LoadTexture(string filePath)
    {
        Texture2D _texture2D;
        byte[] _fileData;

        if (File.Exists(filePath))
        {
            _fileData = File.ReadAllBytes(filePath);
            _texture2D = new Texture2D(2, 2);
            if (_texture2D.LoadImage(_fileData))
            {
                return _texture2D;
            }
        }
        return null;
    }
}
