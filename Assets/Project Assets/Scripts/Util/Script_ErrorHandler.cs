﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

//Manager to handle all the error related events 
//Can be modified to support multiple types of events
public class Script_ErrorHandler : MonoBehaviour
{
    #region Singleton
    public static Script_ErrorHandler Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    [SerializeField]
    private CanvasGroup errorCanvas;

    [SerializeField]
    private Transform[] menuBGElements;

    [SerializeField]
    private Transform icon;

    [SerializeField]
    private Button actionButton;

    [SerializeField]
    private GameObject closeButton;

    [SerializeField]
    private TMP_Text titleTextbox;

    [SerializeField]
    private TMP_Text bodyTextbox;

    [SerializeField]
    private TMP_Text buttonTextbox;

    [SerializeField]
    private AudioClip buttonPressClip;

    #region AnimationParameters
    [Header("Animation Settings")]

    [SerializeField]
    private float canvasFadeTime = 0.5f;

    [SerializeField]
    private float backgroundScaleTime = 0.5f;

    [SerializeField]
    private float iconScaleTime = 0.5f;

    [SerializeField]
    private float iconScaleDelay = 0.1f;

    #endregion

    public void ShowErrorMessage(string titleText, string bodyText, string buttonText, System.Action actionButtonCallBack = null)
    {
        titleTextbox.text = titleText;
        bodyTextbox.text = bodyText;
        if (buttonText != "")
        {
            buttonTextbox.text = buttonText;
        }

        SetActionButton(actionButtonCallBack);
        ResetErrorUI();

        errorCanvas.blocksRaycasts = true;
        errorCanvas.DOFade(1, canvasFadeTime).OnComplete(() =>
        {
            for (int i = 0; i < menuBGElements.Length; i++)
            {
                menuBGElements[i].DOScale(Vector3.one, backgroundScaleTime).SetEase(Ease.OutBack);
            }

            icon.DOScale(Vector3.one, iconScaleTime).SetDelay(iconScaleDelay).SetEase(Ease.OutBack);
            closeButton.SetActive(true);
            if (buttonText != "")
            {
                actionButton.transform.parent.gameObject.SetActive(true);
            }
        });
    }

    public void CloseErrorMessage()
    {
        errorCanvas.DOFade(0, canvasFadeTime).OnComplete(() =>
        {
            errorCanvas.blocksRaycasts = false;
            closeButton.SetActive(false);
            actionButton.transform.parent.gameObject.SetActive(false);
        });
    }

    private void ResetErrorUI()
    {
        for (int i = 0; i < menuBGElements.Length; i++)
        {
            menuBGElements[i].localScale = Vector3.zero;
        }
        icon.localScale = Vector3.zero;
    }

    private void SetActionButton(System.Action actionButtonCallBack = null)
    {
        if (actionButtonCallBack != null)
        {
            actionButton.onClick.RemoveAllListeners();
            actionButton.onClick.AddListener(delegate
            {
                actionButton.transform.parent.GetComponent<Animator>().SetTrigger("Pressed");
                Script_AudioManager.Instance.PlayAudio(buttonPressClip, 1);
                actionButtonCallBack();
            });
        }
    }
}
