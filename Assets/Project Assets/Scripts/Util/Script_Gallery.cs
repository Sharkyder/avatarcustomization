﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.IO;
using UnityEngine.UI;

//For handling screenshot gallery
public class Script_Gallery : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup galleryCanvas;

    [SerializeField]
    private GameObject photoViewer;

    [SerializeField]
    private GameObject leftButton;

    [SerializeField]
    private GameObject rightButton;

    [SerializeField]
    private GameObject closeButton;

    [SerializeField]
    private Transform itemMainImage;

    [SerializeField]
    private Transform itemTempImage;

    [SerializeField]
    private Transform leftPos;

    [SerializeField]
    private Transform midPos;

    [SerializeField]
    private Transform rightPos;

    [SerializeField]
    private AudioClip buttonClip;

    #region AnimationParameters
    [Header("Animation Settings")]

    [SerializeField]
    private float canvasFadeTime = 0.4f;

    [SerializeField]
    private float canvasFadeDelay = 0.5f;

    [SerializeField]
    private float xMoveTime = 0.5f;

    #endregion

    private List<Sprite> screenshotSprites;
    private int currentScreenshotId = 0;
    private bool canMoveToNext = true;

    public void CheckAndLaunchGallery(bool showGallery)
    {
        if (screenshotSprites == null)
        {
            screenshotSprites = new List<Sprite>();
            for (int i = 0; i < Script_UserDataManager.Instance.screenShotGallery.screenshotNames.Count; i++)
            {
                Sprite _sprite = LoadNewSprite(Script_UserDataManager.Instance.screenShotGallery.screenshotNames[i]);
                if (_sprite != null)
                {
                    screenshotSprites.Add(_sprite);
                }
                else
                {
                    //Screenshots are saved by the name
                    //If the name of the file does not exist removes that screenshot from the saved screenshot list for more efficiency
                    Script_UserDataManager.Instance.RemoveScreenShots(Script_UserDataManager.Instance.screenShotGallery.screenshotNames[i]);
                }
            }
            if (screenshotSprites.Count > 0 && showGallery)
            {
                ShowGallery();
            }
        }
        else
        {
            if (screenshotSprites.Count > 0 && showGallery)
            {
                ShowGallery();
            }
        }
    }

    public void AddScreenShot(Sprite sprite)
    {
        CheckAndLaunchGallery(false);
        screenshotSprites.Add(sprite);
    }

    private void ShowGallery()
    {
        Script_AudioManager.Instance.PlayAudio(buttonClip, 1);

        currentScreenshotId = 0;

        galleryCanvas.blocksRaycasts = true;
        galleryCanvas.DOFade(1, canvasFadeTime).OnComplete(() =>
        {
            itemMainImage.GetComponent<Image>().sprite = screenshotSprites[0];

            photoViewer.SetActive(true);
            closeButton.SetActive(true);

            if (screenshotSprites.Count > 1)
            {
                rightButton.SetActive(true);
            }
        });
    }

    private Sprite LoadNewSprite(string filePath, float pixelsPerUnit = 100.0f)
    {
        Sprite _newSprite = null;
        Texture2D _spriteTexture = LoadTexture(filePath);
        if (_spriteTexture == null)
        {
            return null;
        }
        _newSprite = Sprite.Create(_spriteTexture, new Rect(0, 0, _spriteTexture.width, _spriteTexture.height), new Vector2(0.5f, 0.5f), pixelsPerUnit);

        return _newSprite;
    }

    private Texture2D LoadTexture(string filePath)
    {
        Texture2D _texture2D;
        byte[] _fileData;

        if (File.Exists(filePath))
        {
            _fileData = File.ReadAllBytes(filePath);
            _texture2D = new Texture2D(2, 2);
            if (_texture2D.LoadImage(_fileData))
            {
                return _texture2D;
            }
        }
        return null;
    }

    public void CloseGallery()
    {
        photoViewer.GetComponent<Animator>().Play("Base Layer.Exit");
        leftButton.GetComponent<Animator>().Play("Base Layer.Exit");
        rightButton.GetComponent<Animator>().Play("Base Layer.Exit");
        closeButton.GetComponent<Animator>().Play("Base Layer.Exit");

        Script_AudioManager.Instance.PlayAudio(buttonClip, 1);

        galleryCanvas.DOFade(0, canvasFadeTime).SetDelay(canvasFadeDelay).OnComplete(() =>
        {
            photoViewer.SetActive(false);
            leftButton.SetActive(false);
            rightButton.SetActive(false);
            closeButton.SetActive(false);

            galleryCanvas.blocksRaycasts = false;

            rightButton.GetComponent<Button>().enabled = true;
            leftButton.GetComponent<Button>().enabled = true;
        });
    }

    public void OnDirectionalButtonClicked(bool direction)
    {
        if (!canMoveToNext)
        {
            return;
        }
        canMoveToNext = false;

        Script_AudioManager.Instance.PlayAudio(buttonClip, 1);

        if (direction)//true if right button clicked
        {
            OnRightButtonClicked();
        }
        else//false if left button clicked
        {
            OnLeftButtonClicked();
        }
    }

    private void OnRightButtonClicked()
    {
        if (currentScreenshotId + 2 > screenshotSprites.Count - 1)
        {
            ButtonExitAnimation(rightButton);
        }
        if (currentScreenshotId == 0)
        {
            ButtonEnterAnimation(leftButton);
        }

        AnimateMoving(rightPos, screenshotSprites[currentScreenshotId + 1], leftPos, 1);
    }

    private void OnLeftButtonClicked()
    {
        if (currentScreenshotId - 2 < 0)
        {
            ButtonExitAnimation(leftButton);
        }
        if (currentScreenshotId == screenshotSprites.Count - 1)
        {
            ButtonEnterAnimation(rightButton);
        }

        AnimateMoving(leftPos, screenshotSprites[currentScreenshotId - 1], rightPos, -1);
    }

    private void ButtonExitAnimation(GameObject button)
    {
        button.GetComponent<Animator>().Play("Base Layer.Exit");
        button.GetComponent<Button>().enabled = false;
    }

    private void ButtonEnterAnimation(GameObject button)
    {
        button.GetComponent<Button>().enabled = true;
        button.SetActive(false);
        button.SetActive(true);
    }

    private void AnimateMoving(Transform tempStartPos, Sprite currentSprite, Transform movePos, int direction)
    {
        itemTempImage.position = tempStartPos.position;
        itemTempImage.GetComponent<Image>().enabled = true;
        itemTempImage.GetComponent<Image>().sprite = currentSprite;

        itemMainImage.DOMoveX(movePos.position.x, xMoveTime);
        itemTempImage.DOMoveX(midPos.position.x, xMoveTime).OnComplete(() =>
        {
            itemMainImage.transform.position = midPos.position;
            itemMainImage.GetComponent<Image>().sprite = currentSprite;
            itemTempImage.GetComponent<Image>().enabled = false;
            currentScreenshotId += direction;
            currentScreenshotId = Mathf.Clamp(currentScreenshotId, 0, screenshotSprites.Count - 1);
            canMoveToNext = true;
        });
    }
}
