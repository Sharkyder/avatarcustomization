﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

//Handles all the item change related ui and ui events
public class Script_UIManager : MonoBehaviour
{
    #region Singleton
    public static Script_UIManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    //public ItemType currentSelectedType = ItemType.OUTFIT;
    private ItemType currentSelectedType = ItemType.OUTFIT;

    [SerializeField]
    private Image[] itemTypeButtons;

    [SerializeField]
    private Sprite itemTypeActiveSprite;

    [SerializeField]
    private Sprite itemTypeInactiveSprite;

    [SerializeField]
    private TMP_Text coinText;

    [SerializeField]
    private TMP_Text levelText;

    [SerializeField]
    private Transform itemIndicator;

    [SerializeField]
    private Transform itemTick;

    [SerializeField]
    private GameObject equipedButton;

    [SerializeField]
    private GameObject equipButton;

    [SerializeField]
    private GameObject purchaseButton;

    [SerializeField]
    private GameObject levelButton;

    [SerializeField]
    private CanvasGroup faderCanvas;

    [SerializeField]
    private Transform[] menuBGElements;

    [SerializeField]
    private Transform titleBar;

    [SerializeField]
    private Transform house;

    [SerializeField]
    private Transform balloons;

    [SerializeField]
    private GameObject screenshotButton;

    [SerializeField]
    private GameObject galleryButton;

    [SerializeField]
    private Transform goldStatus;

    [SerializeField]
    private Transform levelStatus;

    [SerializeField]
    private AudioClip coinClip;

    [SerializeField]
    private AudioClip errorClip;

    [SerializeField]
    private AudioClip apearClip;

    [SerializeField]
    private AudioClip itemApearClip;

    [SerializeField]
    private AudioClip buttonClip;

    [SerializeField]
    private Transform itemParentTransform;

    [SerializeField]
    private GameObject itemPrefab;


    #region AnimationParameters
    [Header("Animation Settings")]

    [SerializeField]
    private float canvasFadeTime = 0.5f;

    [SerializeField]
    private float canvasFadeDelay = 1f;

    [SerializeField]
    private float backgroundScaleTime = 0.5f;

    [SerializeField]
    private float titleScaleTime = 0.5f;

    [SerializeField]
    private float titleScaleDelay = 0.2f;

    [SerializeField]
    private float houseScaleTime = 0.5f;

    [SerializeField]
    private float houseScaleDelay = 0.4f;

    [SerializeField]
    private float ballonScaleTime = 0.5f;

    [SerializeField]
    private float ballonScaleDelay = 0.5f;

    [SerializeField]
    private float statusMoveTime = 0.5f;

    [SerializeField]
    private float SecondStatusMoveDelay = 0.2f;

    [SerializeField]
    private float itemWaitTime = 0.4f;

    [SerializeField]
    private float itemApearTime = 0.38f;

    [SerializeField]
    private float perItemApearDelay = 0.09f;

    [SerializeField]
    private float indicatorTime = 0.3f;

    [SerializeField]
    private float indicatorRotation = 180f;

    [SerializeField]
    private float tickScaleTIme = 0.3f;

    #endregion


    private List<CharacterSkin> currentItemList;
    private Sequence itemApearSequence;

    private int[] currentSelectedSkinID = new int[System.Enum.GetValues(typeof(ItemType)).Length];

    private bool isItemOnTransition = false;

    private int maxItemCount = 12;

    private List<Script_ItemUIHandler> itemList;

    private void Start()
    {
        itemApearSequence = DOTween.Sequence();

        SpawnItemPrefabs();
        SetCurrentItemList();
    }

    private void SpawnItemPrefabs()
    {
        itemList = new List<Script_ItemUIHandler>();
        for (int i = 0; i < maxItemCount; i++)
        {
            itemList.Add(Instantiate(itemPrefab, itemParentTransform).GetComponent<Script_ItemUIHandler>());
            itemList[i].ResetItemUI();
            itemList[i].itemId = i;
            itemList[i].SetButtonEvents();
        }
    }

    private void SetCurrentItemList()
    {
        switch (currentSelectedType)
        {
            case ItemType.OUTFIT:
                currentItemList = Script_GameDataManager.Instance.outfitList;
                break;

            case ItemType.MOUTH:
                currentItemList = Script_GameDataManager.Instance.mouthList;
                break;

            case ItemType.EYE:
                currentItemList = Script_GameDataManager.Instance.eyeList;
                break;

            default:
                currentItemList = Script_GameDataManager.Instance.outfitList;
                break;
        }
    }

    //Initial apear animations
    public void ShowCreaterUI()
    {
        float _statusY = goldStatus.position.y;
        ResetMenuTransform();

        faderCanvas.DOFade(0, canvasFadeTime).SetDelay(canvasFadeDelay).OnComplete(() =>
        {
            faderCanvas.blocksRaycasts = false;

            for (int i = 0; i < menuBGElements.Length; i++)
            {
                menuBGElements[i].DOScale(Vector3.one, backgroundScaleTime).SetEase(Ease.OutBack);
            }
            Script_AudioManager.Instance.PlayAudio(apearClip, 1);

            titleBar.DOScale(Vector3.one, titleScaleTime).SetDelay(titleScaleDelay).SetEase(Ease.OutBack);
            Script_AudioManager.Instance.PlayDelayAudio(apearClip, 1, titleScaleDelay);

            house.DOScale(Vector3.one, houseScaleTime).SetDelay(houseScaleDelay).SetEase(Ease.OutBack);
            Script_AudioManager.Instance.PlayDelayAudio(apearClip, 1, houseScaleDelay);

            balloons.DOScale(Vector3.one, ballonScaleTime).SetDelay(ballonScaleDelay).SetEase(Ease.OutBack).OnComplete(() =>
            {
                for (int i = 0; i < itemTypeButtons.Length; i++)
                {
                    itemTypeButtons[i].transform.parent.gameObject.SetActive(true);
                }
                Script_AudioManager.Instance.PlayAudio(apearClip, 1);

                equipedButton.SetActive(true);
                galleryButton.SetActive(true);
                screenshotButton.SetActive(true);

                goldStatus.DOMoveY(_statusY, statusMoveTime).SetEase(Ease.OutBack);
                levelStatus.DOMoveY(_statusY, statusMoveTime).SetEase(Ease.OutBack).SetDelay(SecondStatusMoveDelay);

                StartCoroutine(WaitAndLoadItems());
            });
            Script_AudioManager.Instance.PlayDelayAudio(apearClip, 1, ballonScaleDelay);
        });
    }

    private void ResetMenuTransform()
    {
        for (int i = 0; i < menuBGElements.Length; i++)
        {
            menuBGElements[i].localScale = Vector3.zero;
        }
        titleBar.localScale = Vector3.zero;
        house.localScale = Vector3.zero;
        balloons.localScale = Vector3.zero;

        goldStatus.DOMoveY(-100, 0);
        levelStatus.DOMoveY(-100, 0);
    }

    IEnumerator WaitAndLoadItems()
    {
        yield return new WaitForSecondsRealtime(itemWaitTime);
        LoadInitialItems();
    }

    //Gets the saved equiped item for each item type
    private void LoadInitialItems()
    {
        currentSelectedSkinID[0] = Script_GameDataManager.Instance.outfitList.IndexOf(Script_GameDataManager.Instance.outfitList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EqiupedOutfit));
        currentSelectedSkinID[1] = Script_GameDataManager.Instance.mouthList.IndexOf(Script_GameDataManager.Instance.mouthList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedMouth));
        currentSelectedSkinID[2] = Script_GameDataManager.Instance.eyeList.IndexOf(Script_GameDataManager.Instance.eyeList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedEye));

        ShowItemsForType();
    }

    public void ShowItemsForType()
    {
        SetCurrentItemList();

        int _currentItemIndex = 0;
        itemApearSequence = DOTween.Sequence();

        for (int i = 0; i < itemList.Count; i++)
        {
            if (currentItemList.Count > _currentItemIndex)
            {
                SetItem(i, _currentItemIndex);
            }
            else
            {
                //Turn off unnecessary item slots
                itemList[i].SetCanvas(0, false);
            }
            _currentItemIndex++;
        }

        int _currentItemID = currentSelectedSkinID[(int)currentSelectedType];

        //Sets the selected item indictor
        SetItemHighlights(itemIndicator, itemList[_currentItemID].itemSemiParent, itemList[_currentItemID].indicatorSiblingId);

        //Sets the currently equiped item tick
        SetItemHighlights(itemTick, itemList[_currentItemID].itemSemiParent, itemList[_currentItemID].tickSiblingId);

        if (IsItemEquiped())
        {
            equipButton.SetActive(false);
            equipedButton.SetActive(true);
        }
        else
        {
            equipButton.SetActive(true);
            equipedButton.SetActive(false);
        }
    }

    private void SetItem(int listIndex, int itemIndex)
    {
        itemList[listIndex].ResetItemUI();
        if (currentItemList[itemIndex].unlocksAt > Script_UserDataManager.Instance.userData.Level)
        {
            //Show locked by level icon
            itemList[listIndex].lockedIcon.SetActive(true);
        }
        else
        {
            if (currentItemList[itemIndex].itemPrice != 0 && !Script_UserDataManager.Instance.IsItemPurchased(currentItemList[itemIndex].itemName))
            {
                //Show need to purchase icon
                itemList[listIndex].coinIcon.SetActive(true);
            }
        }

        itemList[listIndex].itemImage.sprite = currentItemList[itemIndex].itemUIImage;
        itemList[listIndex].SetCanvas(1, true);

        itemApearSequence.Insert(0, itemList[listIndex].transform.DOScale(Vector3.one, itemApearTime).SetDelay(itemIndex * perItemApearDelay).SetEase(Ease.OutBack));
        Script_AudioManager.Instance.PlayDelayAudio(itemApearClip, 1, itemIndex * perItemApearDelay);
    }

    private void SetItemHighlights(Transform highlight, Transform itemParent, int siblingIndex)
    {
        int _currentItemID = currentSelectedSkinID[(int)currentSelectedType];
        highlight.localScale = Vector3.zero;
        highlight.SetParent(itemParent);
        highlight.SetSiblingIndex(siblingIndex);
        highlight.localPosition = Vector3.zero;
        highlight.localScale = Vector3.one;
    }

    public void OnItemTypeChange(int itemTypeID)
    {
        if (itemTypeID != (int)currentSelectedType && !isItemOnTransition)
        {
            Script_AudioManager.Instance.PlayAudio(buttonClip, 1);

            itemIndicator.localScale = Vector3.zero;
            itemIndicator.SetParent(itemParentTransform.parent);

            for (int i = 0; i < itemTypeButtons.Length; i++)
            {
                itemTypeButtons[i].transform.parent.GetComponent<Animator>().Play("Base Layer.Default");
            }

            itemApearSequence.Complete();

            itemTypeButtons[(int)currentSelectedType].sprite = itemTypeInactiveSprite;
            currentSelectedType = (ItemType)itemTypeID;
            itemTypeButtons[(int)currentSelectedType].sprite = itemTypeActiveSprite;

            itemTypeButtons[(int)currentSelectedType].transform.parent.GetComponent<Animator>().SetTrigger("Pressed");

            ShowItemsForType();
        }
    }

    public void OnItemClicked(int itemID)
    {
        if (currentSelectedSkinID[(int)currentSelectedType] != itemID && !isItemOnTransition)
        {
            AnimateIndicator(itemID);

            Script_CharacterManager.Instance.SetCharacterSkin(currentItemList[itemID]);

            currentSelectedSkinID[(int)currentSelectedType] = itemID;
            int _currentItemID = currentSelectedSkinID[(int)currentSelectedType];

            HideAllBottomButtons();
            if (currentItemList[_currentItemID].unlocksAt > Script_UserDataManager.Instance.userData.Level)
            {
                //If item is locked by level show level requirment
                levelButton.transform.GetComponentInChildren<TMP_Text>().text = "Unlocks at Level " + currentItemList[_currentItemID].unlocksAt.ToString();
                levelButton.SetActive(true);
            }
            else
            {
                if (currentItemList[_currentItemID].itemPrice != 0 && !Script_UserDataManager.Instance.IsItemPurchased(currentItemList[_currentItemID].itemName))
                {
                    //If item needs purchasing show price
                    purchaseButton.transform.GetComponentInChildren<TMP_Text>().text = "Purchase for " + currentItemList[_currentItemID].itemPrice.ToString();
                    purchaseButton.SetActive(true);
                }
                else
                {
                    if (IsItemEquiped())
                    {
                        //If item is equiped show equiped button
                        equipedButton.SetActive(true);
                    }
                    else
                    {
                        //If item is not equiped show equip button
                        equipButton.SetActive(true);
                    }
                }
            }
        }
    }

    private void AnimateIndicator(int itemID)
    {
        isItemOnTransition = true;
        itemIndicator.DOScale(Vector3.zero, indicatorTime);
        itemIndicator.DORotate(new Vector3(0, 0, -indicatorRotation), indicatorTime).OnComplete(() =>
        {
            itemIndicator.transform.SetParent(itemList[itemID].itemSemiParent);
            itemIndicator.transform.SetSiblingIndex(itemList[itemID].indicatorSiblingId);
            itemIndicator.localPosition = Vector3.zero;
            itemIndicator.DOScale(Vector3.one, indicatorTime);
            itemIndicator.DORotate(new Vector3(0, 0, indicatorRotation), indicatorTime).OnComplete(() =>
            {
                isItemOnTransition = false;
            });
        });
    }

    private void HideAllBottomButtons()
    {
        equipButton.SetActive(false);
        equipedButton.SetActive(false);
        purchaseButton.SetActive(false);
        levelButton.SetActive(false);
    }

    public void ShowUserData()
    {
        coinText.text = Script_UserDataManager.Instance.userData.Coins.ToString();
        levelText.text = "Level " + Script_UserDataManager.Instance.userData.Level.ToString();
    }

    public void OnEquipButtonPressed()
    {
        equipButton.GetComponent<Animator>().SetTrigger("Pressed");
        Script_AudioManager.Instance.PlayAudio(buttonClip, 1);

        itemTick.DOScale(Vector3.zero, tickScaleTIme).SetEase(Ease.OutBack).OnComplete(() =>
        {
            switch (currentSelectedType)
            {
                case ItemType.OUTFIT:
                    Script_UserDataManager.Instance.userData.EqiupedOutfit = Script_GameDataManager.Instance.outfitList[currentSelectedSkinID[0]].itemName;
                    break;

                case ItemType.MOUTH:
                    Script_UserDataManager.Instance.userData.EquipedMouth = Script_GameDataManager.Instance.mouthList[currentSelectedSkinID[1]].itemName;
                    break;

                case ItemType.EYE:
                    Script_UserDataManager.Instance.userData.EquipedEye = Script_GameDataManager.Instance.eyeList[currentSelectedSkinID[2]].itemName;
                    break;

                default:
                    //no type selected
                    break;
            }

            Script_UserDataManager.Instance.SaveUserData();

            SetItemTick();

            equipButton.SetActive(false);
            equipedButton.SetActive(true);

            List<AudioClip> _voiceList = Script_GameDataManager.Instance.outfitList[currentSelectedSkinID[0]].voiceList;
            Script_AudioManager.Instance.PlayAudio(_voiceList[Random.Range(0, _voiceList.Count)], 0);
        });
    }

    private void SetItemTick()
    {
        int _currentItemID = currentSelectedSkinID[(int)currentSelectedType];
        itemTick.localScale = Vector3.zero;
        itemTick.SetParent(itemList[_currentItemID].itemSemiParent);
        itemTick.SetSiblingIndex(itemList[_currentItemID].tickSiblingId);
        itemTick.localPosition = Vector3.zero;
        itemTick.DOScale(Vector3.one, tickScaleTIme).SetEase(Ease.OutBack);
    }

    private Transform GetCurrentItemParent()
    {
        int _itemId = 0;
        switch (currentSelectedType)
        {
            case ItemType.OUTFIT:
                _itemId = Script_GameDataManager.Instance.outfitList.IndexOf(Script_GameDataManager.Instance.outfitList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EqiupedOutfit));
                break;

            case ItemType.MOUTH:
                _itemId = Script_GameDataManager.Instance.mouthList.IndexOf(Script_GameDataManager.Instance.mouthList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedMouth));
                break;

            case ItemType.EYE:
                _itemId = Script_GameDataManager.Instance.eyeList.IndexOf(Script_GameDataManager.Instance.eyeList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedEye));
                break;

            default:
                //No type selected : do not happen
                break;
        }

        Transform _currentSelectItemParent = itemList[_itemId].itemSemiParent;
        return _currentSelectItemParent;
    }

    private bool IsItemEquiped()
    {
        bool _isEquiped = false;

        switch (currentSelectedType)
        {
            case ItemType.OUTFIT:
                _isEquiped = Script_GameDataManager.Instance.outfitList[currentSelectedSkinID[0]].itemName == Script_GameDataManager.Instance.outfitList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EqiupedOutfit).itemName;
                break;

            case ItemType.MOUTH:
                _isEquiped = Script_GameDataManager.Instance.mouthList[currentSelectedSkinID[1]].itemName == Script_GameDataManager.Instance.mouthList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedMouth).itemName;
                break;

            case ItemType.EYE:
                _isEquiped = Script_GameDataManager.Instance.eyeList[currentSelectedSkinID[2]].itemName == Script_GameDataManager.Instance.eyeList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedEye).itemName;
                break;

            default:
                //no type selected
                break;
        }

        return _isEquiped;
    }

    public void PurchaseButtonClicked()
    {
        CharacterSkin _item = currentItemList[currentSelectedSkinID[(int)currentSelectedType]];
        if (_item.itemPrice <= Script_UserDataManager.Instance.userData.Coins)
        {
            //If has enough money purcahse
            Script_UserDataManager.Instance.AddItemToPurchaseList(_item.itemName, _item.itemPrice);
            purchaseButton.SetActive(false);
            equipButton.SetActive(true);

            int _currentItemID = currentSelectedSkinID[(int)currentSelectedType];
            itemList[_currentItemID].coinIcon.SetActive(false);
            Script_AudioManager.Instance.PlayAudio(coinClip, 1);
        }
        else
        {
            //Else show not enough coins message
            string _bodyText = "Looks like you don't have \nenough coins";
            Script_ErrorHandler.Instance.ShowErrorMessage("Oops!", _bodyText, "Get Coins", OnAddCoinsButtonClicked);
            Script_AudioManager.Instance.PlayAudio(errorClip, 1);
        }
    }

    public void OnAddCoinsButtonClicked()
    {
        //Sample call back method for error message 
        Debug.Log("Redirect user to store here");
    }
}
