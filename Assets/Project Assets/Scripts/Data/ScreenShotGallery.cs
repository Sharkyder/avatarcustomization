﻿using System.Collections.Generic;

//Data model to store screenshot data, screenshots are saved by their names
[System.Serializable]
public struct ScreenShotGallery
{
    public List<string> screenshotNames;
}