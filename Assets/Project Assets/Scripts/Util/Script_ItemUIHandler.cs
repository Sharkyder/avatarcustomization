using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Handles item prefab data in ui
public class Script_ItemUIHandler : MonoBehaviour
{
    public int itemId = 0;
    public GameObject lockedIcon;
    public GameObject coinIcon;
    public Image itemImage;
    public Transform itemSemiParent;
    public int indicatorSiblingId = 1;
    public int tickSiblingId = 1;

    [SerializeField]
    private AudioClip selectAudioClip;

    [SerializeField]
    private Button itemButton;

    [SerializeField]
    private CanvasGroup itemCanvas;

    public void SetButtonEvents()
    {
        itemButton.onClick.AddListener(delegate { Script_UIManager.Instance.OnItemClicked(itemId); });
        itemButton.onClick.AddListener(delegate { Script_AudioManager.Instance.PlayAudio(selectAudioClip, 1); });
    }

    public void ResetItemUI()
    {
        transform.localScale = Vector3.zero;
        lockedIcon.SetActive(false);
        coinIcon.SetActive(false);
    }

    public void SetCanvas(int alpha, bool blocksRaycasts)
    {
        itemCanvas.alpha = alpha;
        itemCanvas.blocksRaycasts = blocksRaycasts;
    }

}
