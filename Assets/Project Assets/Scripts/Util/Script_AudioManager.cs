﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Manager for all the audio in the scene
public class Script_AudioManager : MonoBehaviour
{
    #region Singleton
    public static Script_AudioManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    //0 - character, 1 - buttonclick, can add more layers
    [SerializeField]
    private AudioSource[] audioSourceLayers;

    public void PlayAudio(AudioClip clip, int audioLayerID)
    {
        audioSourceLayers[audioLayerID].Stop();
        audioSourceLayers[audioLayerID].clip = clip;
        audioSourceLayers[audioLayerID].Play();
    }

    //Delayed playing of audios to support delayed animations
    public void PlayDelayAudio(AudioClip clip, int audioLayerID, float delay)
    {
        StartCoroutine(PlayDelayedAudio(clip, audioLayerID, delay));
    }

    IEnumerator PlayDelayedAudio(AudioClip clip, int audioLayerID, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        PlayAudio(clip, audioLayerID);
    }
}
