﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

//Custom editor window to create character skin scriptable objects
public class CharacterSkinCreater : EditorWindow
{
    public string itemName = "item_name";
    public string displayName = "display_name";

    public ItemType itemType = ItemType.OUTFIT;
    public int unlocskAt = 0;
    public int itemPrice = 0;

    public Sprite itemImage;
    public Sprite itemUIImage;


    [MenuItem("Window/Skin Creator")]
    public static void ShowWindow()
    {
        GetWindow<CharacterSkinCreater>("Create Skin");
    }

    private void OnGUI()
    {
        itemName = EditorGUILayout.TextField("Item Name ", itemName);
        displayName = EditorGUILayout.TextField("Display Name ", displayName);

        itemType = (ItemType)EditorGUILayout.EnumPopup("Item Type ", itemType);

        unlocskAt = EditorGUILayout.IntField("Unlocks At ", unlocskAt);
        itemPrice = EditorGUILayout.IntField("Item Price ", itemPrice);

        itemImage = (Sprite)EditorGUILayout.ObjectField("Item Image ", itemImage, typeof(Sprite), allowSceneObjects: false);
        itemUIImage = (Sprite)EditorGUILayout.ObjectField("Item UI Image ", itemUIImage, typeof(Sprite), allowSceneObjects: false);

        if (GUILayout.Button("Create Skin"))
        {
            CharacterSkin _skinAsset = ScriptableObject.CreateInstance<CharacterSkin>();
            _skinAsset.itemName = itemName;
            _skinAsset.displayName = displayName;
            _skinAsset.itemType = itemType;
            _skinAsset.unlocksAt = unlocskAt;
            _skinAsset.itemPrice = itemPrice;
            _skinAsset.itemImage = itemImage;
            _skinAsset.itemUIImage = itemUIImage;

            Type _projectWindowUtilType = typeof(ProjectWindowUtil);
            MethodInfo _getActiveFolderPath = _projectWindowUtilType.GetMethod("GetActiveFolderPath", BindingFlags.Static | BindingFlags.NonPublic);
            object _obj = _getActiveFolderPath.Invoke(null, new object[0]);
            string _pathToCurrentFolder = _obj.ToString();

            string _assetName = UnityEditor.AssetDatabase.GenerateUniqueAssetPath(_pathToCurrentFolder + "/ " + itemName + ".asset");
            AssetDatabase.CreateAsset(_skinAsset, _assetName);
            AssetDatabase.SaveAssets();

            Selection.activeObject = _skinAsset;

            EditorUtility.FocusProjectWindow();
        }
    }
}
