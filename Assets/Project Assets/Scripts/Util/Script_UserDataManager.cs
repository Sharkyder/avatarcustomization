﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Manages saving and loading of user data
public class Script_UserDataManager : MonoBehaviour
{
    #region Singleton
    public static Script_UserDataManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    [HideInInspector]
    public UserData userData;

    [HideInInspector]
    public ScreenShotGallery screenShotGallery;

    [HideInInspector]
    public const string USERDATASTRING = "USERDATA";

    [HideInInspector]
    public const string SCREENSHOTSTRING = "SCREENSHOTS";

    private void Start()
    {
        LoadUserData();
    }

    private void LoadUserData()
    {
        LoadJsonData();

        Script_UIManager.Instance.ShowCreaterUI();
        Script_UIManager.Instance.ShowUserData();
        Script_CharacterManager.Instance.LoadCharacterSkin();
    }

    public void LoadJsonData()
    {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.GetString(USERDATASTRING, "nodata") == "nodata")
        {
            //No previous data : first time using the app
            userData = UserDataGetter.GetUserData();

            //Sets the equiped item to first item of each item type list
            if (Script_GameDataManager.Instance.outfitList.Count > 0)
            {
                userData.EqiupedOutfit = Script_GameDataManager.Instance.outfitList[0].itemName;
            }
            if (Script_GameDataManager.Instance.mouthList.Count > 0)
            {
                userData.EquipedMouth = Script_GameDataManager.Instance.mouthList[0].itemName;
            }
            if (Script_GameDataManager.Instance.eyeList.Count > 0)
            {
                userData.EquipedEye = Script_GameDataManager.Instance.eyeList[0].itemName;
            }

            PlayerPrefs.SetString(USERDATASTRING, JsonUtility.ToJson(userData));
        }
        else
        {
            userData = JsonUtility.FromJson<UserData>(PlayerPrefs.GetString(USERDATASTRING));
        }

        if (PlayerPrefs.GetString(SCREENSHOTSTRING, "nodata") == "nodata")
        {
            //no previous data
            screenShotGallery = new ScreenShotGallery();
            screenShotGallery.screenshotNames = new List<string>();

            PlayerPrefs.SetString(SCREENSHOTSTRING, JsonUtility.ToJson(screenShotGallery));
        }
        else
        {
            screenShotGallery = JsonUtility.FromJson<ScreenShotGallery>(PlayerPrefs.GetString(SCREENSHOTSTRING));
        }
    }

    public void SaveUserData()
    {
        PlayerPrefs.SetString(USERDATASTRING, JsonUtility.ToJson(userData));
    }

    public void AddScreenShots(string screenshotName)
    {
        screenShotGallery.screenshotNames.Add(screenshotName);
        PlayerPrefs.SetString(SCREENSHOTSTRING, JsonUtility.ToJson(screenShotGallery));
    }

    public void RemoveScreenShots(string screenshotName)
    {
        screenShotGallery.screenshotNames.Remove(screenshotName);
        PlayerPrefs.SetString(SCREENSHOTSTRING, JsonUtility.ToJson(screenShotGallery));
    }

    //Check if an item is purchased by checking its name
    public bool IsItemPurchased(string itemName)
    {
        bool _itemPurchased = false;
        for (int i = 0; i < userData.PurchasedItems.Count; i++)
        {
            if (userData.PurchasedItems[i].itemName == itemName)
            {
                _itemPurchased = true;
            }
        }
        return _itemPurchased;
    }

    public void AddItemToPurchaseList(string itemName, int itemPrice)
    {
        PurchasedItem _purchasedItem = new PurchasedItem();
        _purchasedItem.itemName = itemName;
        userData.PurchasedItems.Add(_purchasedItem);

        userData.Coins -= itemPrice;
        Script_UIManager.Instance.ShowUserData();

        SaveUserData();
    }
}
