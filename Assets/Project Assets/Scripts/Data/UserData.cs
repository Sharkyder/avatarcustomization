using System.Collections;
using System.Collections.Generic;

//Modified user data class
[System.Serializable]
public struct UserData
{
    public int Level;
    public int Coins;
    public List<PurchasedItem> PurchasedItems;
    public string EqiupedOutfit;
    public string EquipedMouth;
    public string EquipedEye;
}