﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Scriptable object class to create character items
[CreateAssetMenu(fileName = "New CharacterSkin", menuName = "Scriptable Objects/Character Skin")]
public class CharacterSkin : ScriptableObject
{
    public string itemName;
    public string displayName;//Can be use to display item names to players, not used in the demo
    public ItemType itemType;
    public int unlocksAt;
    public int itemPrice;
    public Sprite itemImage;
    public Sprite itemUIImage;
    public List<AudioClip> voiceList;
}

public enum ItemType { OUTFIT = 0, MOUTH = 1, EYE = 2 }
