﻿using UnityEngine;
using UnityEditor;

//Custor inspector to modify saved user data values on editor
//Change saved level/coins of player
//Reset purchased items list
[CustomEditor(typeof(Script_UserDataManager))]
public class UserDataEditor : Editor
{
    public int Level = 0;
    public int Coins = 0;

    private bool isUserDataSet = false;
    private UserData userData;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (!isUserDataSet)
        {
            isUserDataSet = true;
            Script_UserDataManager _userManager = (Script_UserDataManager)target;
            _userManager.LoadJsonData();
            userData = _userManager.userData;

            Level = userData.Level;
            Coins = userData.Coins;
        }

        Level = EditorGUILayout.IntField("Level ", Level);
        Coins = EditorGUILayout.IntField("Coins ", Coins);

        if (GUILayout.Button("Update User Data"))
        {
            userData.Level = Level;
            userData.Coins = Coins;

            PlayerPrefs.SetString(Script_UserDataManager.USERDATASTRING, JsonUtility.ToJson(userData));
            Debug.Log("User Data Updated!");
        }

        if (GUILayout.Button("Clear Purchase List"))
        {
            userData.PurchasedItems = new System.Collections.Generic.List<PurchasedItem>();
            Script_GameDataManager _dataManager = ((MonoBehaviour)target).gameObject.GetComponent<Script_GameDataManager>();
            if (_dataManager.outfitList.Count > 0)
            {
                userData.EqiupedOutfit = _dataManager.outfitList[0].itemName;
            }
            if (_dataManager.mouthList.Count > 0)
            {
                userData.EquipedMouth = _dataManager.mouthList[0].itemName;
            }
            if (_dataManager.eyeList.Count > 0)
            {
                userData.EquipedEye = _dataManager.eyeList[0].itemName;
            }

            PlayerPrefs.SetString(Script_UserDataManager.USERDATASTRING, JsonUtility.ToJson(userData));
            Debug.Log("Purchase Data Cleared!");
        }
    }
}
