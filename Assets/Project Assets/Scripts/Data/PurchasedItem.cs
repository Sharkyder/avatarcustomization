﻿//Data model to store item purchase data
//Can be modified to add more purchase related data
//Ex : Purchase date
[System.Serializable]
public struct PurchasedItem
{
    public string itemName;
}
