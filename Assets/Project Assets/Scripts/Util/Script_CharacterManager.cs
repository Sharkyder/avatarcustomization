﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Manager to handle character model related events
public class Script_CharacterManager : MonoBehaviour
{
    #region Singleton
    public static Script_CharacterManager Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    [SerializeField]
    private SpriteRenderer characterOutfit;

    [SerializeField]
    private SpriteRenderer characterMouth;

    [SerializeField]
    private SpriteRenderer characterEyes;

    public void LoadCharacterSkin()
    {
        characterOutfit.sprite = Script_GameDataManager.Instance.outfitList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EqiupedOutfit).itemImage;
        characterMouth.sprite = Script_GameDataManager.Instance.mouthList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedMouth).itemImage;
        characterEyes.sprite = Script_GameDataManager.Instance.eyeList.Find((x) => x.name == Script_UserDataManager.Instance.userData.EquipedEye).itemImage;
    }

    public void SetCharacterSkin(CharacterSkin skin)
    {
        switch (skin.itemType)
        {
            case ItemType.OUTFIT:
                characterOutfit.sprite = skin.itemImage;
                break;

            case ItemType.MOUTH:
                characterMouth.sprite = skin.itemImage;
                break;

            case ItemType.EYE:
                characterEyes.sprite = skin.itemImage;
                break;

            default:
                //No item type : do not happen
                break;
        }
    }
}
